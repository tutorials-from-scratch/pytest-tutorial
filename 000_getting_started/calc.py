"""This module contains a simple mathematical functions.
"""


def add(first: int, second: int) -> int:
    """function returns the sum of two arguments
    """
    return first + second
