"""This module test case for calc
"""
from calc import add


def test_add():
    """ function to test add()
    """
    result = add(5, 6)
    assert result == 11
